# Ractive Webpack Boilerplate

Boilerplate repo for a few of my *favorite things*:

* [ES6 (via BabelJS)](http://babeljs.io/)
* [Ractive](http://www.ractivejs.org/)
* [Webpack](http://webpack.github.io)
* [i18next](http://i18next.com/)
* [lodash](https://lodash.com/docshttps://github.com/chjj/marked)
* [markdown-it](https://markdown-it.github.io)
* [Moment.js](http://momentjs.com/)
* [xhttp](https://github.com/Mitranim/xhttp)
* [Ractive-route](https://github.com/MartinKolarik/ractive-route)
* [Animate.css](https://daneden.github.io/animate.css/)
* [Bootstrap](http://getbootstrap.com/)
* [Font-Awesome](http://fontawesome.io/)
* **TODO**: Add pattern for LocalStorage-backed data.

![](Readme.jpg)

----

## Install & Run:

### For development with hot reloading:

    npm install
    node app.js

Open: http://localhost:3000

### For production bundle:

    webpack -p
    node app.js

Open: http://localhost:3000
